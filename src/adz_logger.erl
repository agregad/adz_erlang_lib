-module(adz_logger).
-vsn("1.0").
-behaviour(gen_server).

%% API
-export([start_link/1, stop/0, log/1, log_write/2, get_filename/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%%====================================================================
%% API
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the server
%%--------------------------------------------------------------------
start_link(Filename) ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [Filename], []).

%%--------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Функция инициализации
%%--------------------------------------------------------------------
init(Filename) ->
  {ok, Filename}.

stop() ->
  gen_server:cast(?MODULE, stop).

log(S) when is_binary(S) ->
  log(binary_to_list(S));
log(S) ->
  gen_server:cast(?MODULE, {log, S}).

get_filename() ->
  gen_server:call(?MODULE, filename).

%%--------------------------------------------------------------------
%% Function: %% handle_call(Request, From, State) -> {reply, Reply, State} |
%%                                      {reply, Reply, State, Timeout} |
%%                                      {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, Reply, State} |
%%                                      {stop, Reason, State}
%% Description: Handling call messages
%%--------------------------------------------------------------------
handle_call(filename, _From, State) ->
  {reply, State, State}.

%%--------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%--------------------------------------------------------------------
handle_cast(stop, State) ->
  {stop, normal, State};
handle_cast({log, S}, Filename) ->
  log_write(Filename, S),
  {noreply, Filename}.

%%--------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%====================================================================
%% Functions
%%====================================================================
log_write(Filename, S) ->
  Msg = io_lib:format("[~p] ~p~n", [get_date_time(), lists:flatten(S)]),
  file:write_file(Filename, lists:flatten(Msg), [append]).

get_date_time() ->
  {H, Min, S} = time(),
  {Y, M, D} = date(),
  lists:flatten(io_lib:format("~p-~p-~p ~p:~p:~p", [Y, M, D, H, Min, S])).
