-module(adz_redis).
-vsn("1.0").

%% API
-export([start_link/0, stop/0, q/1, q/2, fq/1, fq/2]).
-export([exists/1, keys/1, rename/2, set/2, get/1, get/2, delete/1, type/1, flushall/0]).
-export([lpush/2, rpop/1]).
-export([zadd/3, zcard/1, zinterstore/2, zrange/1, zrange/3, zrevrange/1, zrevrange/3, zrange_withscores/1, zrange_withscores/3]).
-export([sadd/2, spop/1, sismember/2, smembers/1]).

%% =========================================
%% Старт/стоп процесса
%% =========================================

%% Запуск процесса
start_link() ->
  case whereis(?MODULE) of
    undefined ->
      case eredis:start_link() of
        {ok, Client} ->
          register(?MODULE, Client),
          {ok, Client};
        {error, Reason} ->
          {error, Reason}
      end;
    Pid ->
      {ok, Pid}
  end.

stop() ->
  eredis:stop(get_client()).

%% =========================================
%% Работа с key-value значениями
%% =========================================

%% <<"1">> -> true;
%% <<"0">> -> false
exists(Key) ->
  fq(["EXISTS", Key]).

%% <<"OK">>
set(Key, Value) ->
  fq(["SET", Key, Value]).

get(Key) ->
  get(Key, null).
get(Key, Default) ->
  case exists(Key) of
    <<"1">> -> fq(["GET", Key]);
    <<"0">> -> Default
  end.

delete(Key)->
  fq(["DEL", Key]).

type(Key) ->
  fq(["TYPE", Key]).

lpush(Key, Value) ->
  fq(["LPUSH", Key, Value]).

%% <<"value">>
%% undefined
rpop(Key) ->
  fq(["RPOP", Key]).

zadd(Key, Score, Member) ->
  fq(["ZADD", Key, Score, Member]).

zinterstore(Output, Keys) ->
  fq(lists:append(["ZINTERSTORE", Output, length(Keys)], Keys)).

zrevrange(Key) ->
  zrevrange(Key, 0, -1).
zrevrange(Key, Start, Stop) ->
  fq(["ZREVRANGE", Key, Start, Stop]).

zrange(Key) ->
  zrange(Key, 0, -1).
zrange(Key, Start, Stop) ->
  fq(["ZRANGE", Key, Start, Stop]).

zrange_withscores(Key) ->
  zrange_withscores(Key, 0, -1).
zrange_withscores(Key, Start, Stop) ->
  fq(["ZRANGE", Key, Start, Stop, "WITHSCORES"]).

zcard(Key) ->
  fq(["ZCARD", Key]).

sadd(Key, Member) ->
  fq(["SADD", Key, Member]).

spop(Key) ->
  fq(["SPOP", Key]).

sismember(Key, Member) ->
  fq(["SISMEMBER", Key, Member]).

smembers(Key) ->
  fq(["SMEMBERS", Key]).

keys(Pattern) ->
  fq(["KEYS", Pattern]).

rename(Key, Newkey) ->
  fq(["RENAME", Key, Newkey]).

flushall() ->
  fq(["FLUSHALL"]).


fq(Command) ->
  {ok, Value} = q(Command),
  Value.

fq(Command, Timeout) ->
  {ok, Value} = q(Command, Timeout),
  Value.

q(Command) ->
  eredis:q(get_client(), Command).

q(Command, Timeout) ->
  eredis:q(get_client(), Command, Timeout).

get_client() ->
  whereis(?MODULE).
