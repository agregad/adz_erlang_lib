-module(adz_cron).
-vsn("1.0").
-behaviour(gen_server).

%% API
-export([start_link/2, stop/0, run/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%%====================================================================
%% API
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the server
%%--------------------------------------------------------------------
start_link(Callback, Timeout) ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [Callback, Timeout], []).

%%--------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Функция инициализации
%%--------------------------------------------------------------------
init([Callback, Timeout]) ->
  State = timer_start(Timeout),
  {ok, {State, Timeout, Callback}}.

stop() ->
  gen_server:cast(?MODULE, stop).

run() ->
  gen_server:call(?MODULE, run).

%%--------------------------------------------------------------------
%% Function: %% handle_call(Request, From, State) -> {reply, Reply, State} |
%%                                      {reply, Reply, State, Timeout} |
%%                                      {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, Reply, State} |
%%                                      {stop, Reason, State}
%% Description: Handling call messages
%%--------------------------------------------------------------------
handle_call(run, _From, {OldTimeRef, Timeout, {Mod, Fun, Args}}) ->
  timer_stop(OldTimeRef),
  erlang:apply(Mod, Fun, Args),
  TimeRef = timer_start(Timeout),
  {reply, ok, {TimeRef, Timeout, {Mod, Fun, Args}}}.

%%--------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%--------------------------------------------------------------------
handle_cast(stop, {OldTimeRef, Timeout, Callback}) ->
  timer_stop(OldTimeRef),
  {stop, normal, {undefined, Timeout, Callback}};
handle_cast(_Msg, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%====================================================================
%% Functions
%%====================================================================
timer_start(Timeout) ->
  {ok, TimeRef} = timer:apply_after(Timeout, ?MODULE, run, []),
  TimeRef.

timer_stop(TimeRef) ->
  case TimeRef of
    undefined -> ok;
    _ -> timer:cancel(TimeRef)
  end.
